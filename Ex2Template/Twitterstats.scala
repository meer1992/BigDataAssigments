import org.apache.log4j.{Level, Logger}
import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._
import org.apache.spark.streaming.StreamingContext._
import java.io._
import java.util.Locale
import java.text._
import java.net._
import java.util.Calendar
import org.apache.tika.language.LanguageIdentifier
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.parsers.DocumentBuilder
import org.w3c.dom.Document

// Check example https://github.com/apache/bahir/blob/master/streaming-twitter/examples/src/main/scala/org/apache/spark/examples/streaming/twitter/TwitterPopularTags.scala

object Twitterstats
{ 
	def getTimeStamp() : String =
	{
		return new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime())
	}
	
	def getLang(s: String) : String =
	{
		val inputStr = s.replaceFirst("RT", "").replaceAll("@\\p{L}+", "").replaceAll("https?://\\S+\\s?", "")
		var langCode = new LanguageIdentifier(inputStr).getLanguage
		
		// Detect if japanese
		var pat = Pattern.compile("\\p{InHiragana}") 
		var m = pat.matcher(inputStr)
		if (langCode == "lt" && m.find)
			langCode = "ja"
		// Detect if korean
		pat = Pattern.compile("\\p{IsHangul}");
		m = pat.matcher(inputStr)
		if (langCode == "lt" && m.find)
			langCode = "ko"
		
		return langCode
	}
  
	def getLangNameFromCode(code: String) : String =
	{
		return new Locale(code).getDisplayLanguage(Locale.ENGLISH)
	}

	// id, username, max_count, min_count, isRetweet, rt_text

	def printTweets(tweets: Array[(Long, (Int, Int, String, String))])
	{
		for( i <- 0 until tweets.size)
		{		
			val id = tweets(i)._1
			val value = tweets(i)._2
			println(id + ", "+ value._1)
		}
		println("\n-----------------------------------\n")
	}

	var firstTime = true
	var t0: Long = 0
	val pw = new java.io.PrintWriter(new File("twitterLog.csv"))

	def write2Log(tweets: Array[(Long,(Int))])
	{
		if (firstTime)
		{
			pw.write("Seconds,Language,Language-code,TotalLangCount,ID,MaxCount,MinCount,Count,Text\n")
			t0 = System.currentTimeMillis
			firstTime = false
		}
		else
		{
			val seconds = (System.currentTimeMillis - t0) / 1000
			
			if (seconds < 30)
			{
				println("\nElapsed time = " + seconds + " seconds. Logging will be started after 60 seconds.")
				return
			}
			
			println("Logging the output to the log file\nElapsed time = " + seconds + " seconds\n-----------------------------------")
			if(seconds > 30 && seconds < 32) {
			for(i <-0 until tweets.size)
				{
				val id = tweets(i)._1
				val counts = tweets(i)._2
				
				// We enclose seconds with brackets because otherwise languages like persian would write seconds in reverse!
				pw.write(id +"," + counts + "\n")
			}
			}
			
		}
	}

	def getHashTags(str: String):List[String] ={
		 val pattern = "(?:\\s|\\A|^)[##]+([A-Za-z0-9-_]+)".r
		 var hashtags = List[String]()
         for(m <- pattern.findAllIn(str)) { 
            hashtags = m.replaceAll("\\s", "") :: hashtags
          }
          return hashtags
	}


	def main(args: Array[String])
	{
		val file = new File("cred.xml")
		val documentBuilderFactory = DocumentBuilderFactory.newInstance
		val documentBuilder = documentBuilderFactory.newDocumentBuilder
		val document = documentBuilder.parse(file);
			
		// Configure Twitter credentials
		val consumerKey = document.getElementsByTagName("consumerKey").item(0).getTextContent 				
		val consumerSecret = document.getElementsByTagName("consumerSecret").item(0).getTextContent 		
		val accessToken = document.getElementsByTagName("accessToken").item(0).getTextContent 				
		val accessTokenSecret = document.getElementsByTagName("accessTokenSecret").item(0).getTextContent	
		
		Logger.getLogger("org").setLevel(Level.OFF)
		Logger.getLogger("akka").setLevel(Level.OFF)
		Logger.getRootLogger.setLevel(Level.OFF)

		// Set the system properties so that Twitter4j library used by twitter stream
		// can use them to generate OAuth credentials
		System.setProperty("twitter4j.oauth.consumerKey", consumerKey)
		System.setProperty("twitter4j.oauth.consumerSecret", consumerSecret)
		System.setProperty("twitter4j.oauth.accessToken", accessToken)
		System.setProperty("twitter4j.oauth.accessTokenSecret", accessTokenSecret)

		val sparkConf = new SparkConf().setAppName("TwitterPopularTags")

		val sc = new SparkContext(sparkConf)

		val ssc = new StreamingContext(sparkConf, Seconds(2))
		val tweets = TwitterUtils.createStream(ssc, None)
		
		// Insert your code here
		val english_tweets = tweets.filter(status => getLang(status.getText)=="en"||getLang(status.getText)=="no")
		val mapped_tweets = english_tweets.map(status => (if(status.isRetweet()) status.getRetweetedStatus().getId() else status.getId(),
															(if(status.isRetweet()) status.getRetweetedStatus().getUser.getScreenName else status.getUser.getScreenName, 
															if(status.isRetweet()) status.getRetweetedStatus().getRetweetCount else status.getRetweetCount,
															// status.isRetweet(),
															if(status.isRetweet()) status.getRetweetedStatus().getRetweetCount else status.getRetweetCount,
															if(status.isRetweet()) status.getRetweetedStatus().getText() else status.getText)))
		


		// id, username, max_counter_tweet, min_counter_tweet, text
		val transformed_ = mapped_tweets.transform(rdd => rdd.sortByKey()).reduceByKeyAndWindow((a:(String, Int, Int, String), b:(String, Int, Int, String)) => 
			(a._1, math.max(a._2, b._2), math.min(a._2, b._2), a._4), Seconds(30), Seconds(2))

		// id, username, tweet_count, text
		// val tweets_counts = transformed_.map{case (id, (username, max_count, min_count, text)) => (id, (username, max_count - min_count + 1, text))}

		transformed_.foreachRDD(rdd => rdd.cache())

		val ds = transformed_.map(x => (x._1, (x._2._2 - x._2._3 + 1))).transform(rdd => rdd.reduceByKey(_ + _))

		var joined_d = transformed_.join(ds)

		var hashtags = joined_d.map(x => (x._1, (x._2._1._1, x._2._2, x._2._1._4, getHashTags(x._2._1._4))))

		var hashtags = joined_d.map(x => (x._1, (x._2._1._1, x._2._2, x._2._1._4, getHashTags(x._2._1._4))))

		// var rdd = sc.parallelize(hashtags)

		// var hashtags_list = hashtags.map(x => (x._1, x._2._4.map(y => y)))



		var hashtags_keys = hashtags.map(x => if(x._2._4.size == 0) (x._1, (x._2._1, x._2._2, x._2._3, 0)) else 
		x._2._4.map(y => (y, (x._2._1, x._2._2, x._2._3, 1))))


		// hashtags_list.print()

		// val filtered_hashtags = hashtags.filter()
		// using id as key if hashtags lists is empty
		// var hashtags_keys = hashtags.map(x => if(x._2._4.size == 0)  (x._1, (x._2._1, x._2._2, x._2._3, 0)))
		      // else x._2._4.map(y => (y, (x._2._1, x._2._2, x._2._3, 1))))

		// val hashtags_counts = hashtags_keys.map(x => (x._1, x._2._4))

		// hashtags_keys.print()

		// joined_d.print()

		// ds.foreachRDD(rdd => write2Log(rdd.collect))

		// #hashtag adding..
		// if(status.isRetweet()) status.getRetweetedStatus().getText() else status.getText
		// val enTweetsDStream = english_tweets.

		// val sorted_ = enTweetsDStream.transform(rdd => rdd.sortByKey())

		// val tweet_count = sorted_.reduceByKeyAndWindow((a:(String, Int, Int, Boolean, String), b:(String, Int, Int, Boolean, String)) => 
		// 	(a._1, math.max(a._2, b._2), math.min(a._2, b._2), a._4, a._5), Seconds(120), Seconds(5))


	 	// id, username, max_count, min_count, isRetweet, rt_text
	 	// val
	 	// tweets_counts.print()
	 	// printTweets(transformed_)
	 	// transformed_.print() 
		// counts.foreachRDD(rdd => printTweets(rdd.collect))

		ssc.start()
		ssc.awaitTermination()
	}
}

