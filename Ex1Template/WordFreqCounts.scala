// sbt assembly help: https://sparkour.urizone.net/recipes/building-sbt/
import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j.Logger
import org.apache.log4j.Level
import java.io._
import scala.util.matching.Regex
import java.util.Locale
import org.apache.commons.lang3.StringUtils

case class Bigram(firstWord: String,f_anySpecialChar:Boolean, secondWord: String, nd_anySpecialChar:Boolean) {
	def isValidBigram: Boolean = !firstWord.isEmpty && !secondWord.isEmpty
	def anySepcialChar: Boolean = !f_anySpecialChar && !nd_anySpecialChar
}

//replaceAll("^[^a-zA-Z].*?[^a-zA-Z]$|^[a-zA-Z].*?[^a-zA-Z]$|^[^a-zA-Z].*?[a-zA-Z]$",""
//replaceAll("^[^a-zA-Z].*?[^a-zA-Z]$|^[a-zA-Z].*?[^a-zA-Z]$|^[^a-zA-Z].*?[a-zA-Z]$","")
object Bigram {
  def apply(input: String): List[Bigram] = {
    val splitWords = (input.split(" "):+ "itsendoftheline").sliding(2).toList
    val reg_ =  "^[^a-zA-Z].*?[^a-zA-Z]$|^[a-zA-Z].*?[^a-zA-Z]$|^[^a-zA-Z].*?[a-zA-Z]$"
    val reg = "^[^a-zA-z]*|[^a-zA-Z]*$"
    splitWords.map(words => 
    	if(words.size > 1) new Bigram(words(0).trim.toLowerCase.replaceAll(reg,""),words(0).trim.toLowerCase.matches(reg_),words(1).trim.toLowerCase.replaceAll(reg,""),words(1).trim.toLowerCase.matches(reg_)) else if(words.size == 1) new Bigram(words(0).trim.toLowerCase.replaceAll(reg,""),words(0).trim.toLowerCase.matches(reg_),"",false) else new Bigram("",false,"",false))
  }
}

object WordFreqCounts 
{

	// def stringToBigrams(s: String) = {
 //    val words = s.split(" ")
 //    if (words.size >= 2) {
 //      words.sliding(2).map(a => Bigram(a(0), a(1)))
 //    } 
 //  }
 	// def countPres(list : List) {
 	// 	val counter = list.groupBy(identity).mapValues(_.size)
 	// }

	def main(args: Array[String]) 
	{
		val inputFile = args(0) // Get input file's name from this command line argument
		val outputFile = "output.txt"
		val output_File = "output2.txt"
		val conf = new SparkConf().setAppName("WordFreqCounts")
		val sc = new SparkContext(conf)
		
		println("Input file: " + inputFile)
		// matching a word starts with char and ends with char
		
		// Uncomment these two lines if you want to see a less verbose messages from Spark
		//Logger.getLogger("org").setLevel(Level.OFF);
		//Logger.getLogger("akka").setLevel(Level.OFF);
		
		val t0 = System.currentTimeMillis

		val input =  sc.textFile(inputFile)
		// each word sepeared by whitespace
		// val words = input.flatMap(line => line.split(" ").sliding(2)).map(words => if(words.size > 1) List(words(0).trim.toLowerCase.replaceAll("^[^a-zA-Z].*?[^a-zA-Z]$|^[a-zA-Z].*?[^a-zA-Z]$|^[^a-zA-Z].*?[a-zA-Z]$",""),words(1).trim.toLowerCase.replaceAll("^[^a-zA-Z].*?[^a-zA-Z]$|^[a-zA-Z].*?[^a-zA-Z]$|^[^a-zA-Z].*?[a-zA-Z]$","")))
		val words = input.flatMap(Bigram.apply).filter(_.isValidBigram).filter(_.anySepcialChar)
																// _.trim.toLowerCase
																// .replaceAll("^[^a-zA-Z].*?[^a-zA-Z]$|^[a-zA-Z].*?[^a-zA-Z]$|^[^a-zA-Z].*?[a-zA-Z]$",""))
																// .filter(!_.isEmpty)
																// .map(_.)                       // take continuous pairs
													   //     //     	.filter(_.size == 2)              // sliding can return partial
													   //     //     	.map{ case Array(a, b) => ((a, b), 1) })
     				
     											// 				// .flatMap(x => x)
		
		// val bgcount = words.map(bg => (bg.firstWord, bg.secondWord))
     	// val grouping =  bgcount.groupByKey()

     	val counts = words.map(word => (word.firstWord, 1)).reduceByKey{case (x, y) => x + y}
     	// precedence words
     	val filtered = words.filter(word => word.secondWord != "itsendoftheline").map(bg => (bg.secondWord, bg.firstWord))
     	val grouping = filtered.groupByKey

     	val counting_pres  = grouping.map(x => (x._1, x._2.toList.groupBy(identity).mapValues(_.size).toSeq.sortWith(_._1 < _._1).sortWith(_._2 > _._2)))//.))toSeq.sortBy(_._1)

     	

     	val sorted_list = counts.sortByKey().sortBy(_._2,false)

		val converted_toString = counting_pres.map(x => (x._1,x._2.mkString))
     	val converted_counts = sorted_list.map(x => (x._1, x._2.toString))

     	val joined_d = converted_counts.join(converted_toString).sortByKey().sortBy(_._2._1.toInt,false)

     	val format_data = joined_d.map(x => x._1 +":" + x._2._1 + "\n\t" + x._2._2.replace(")(","\n\t").replace("(","").replace(")","").replace(",",":"))

     	// val tes = counting_pres.map(x=> (sorted_list.map(g=> (g._1 == x._1)))
     	// val tes = sorted_list.zip(counts_secondDataset).map(vk => (vk._1._1, vk._1._2, vk._2._2))

     	// val dsds = sorted_list.join(counting_pres)

       	val str = sorted_list.map(x => x._1 + ":" + x._2)

     	// val gg = counts.zip(counting_pres)//.map(vk => (vk._1._1, vk._1._2, vk._2._2))
		// import spark.implicits._
  //    	val gg = counts.toDF("type", "value")
  //    	val nn = counting_pres.toDF("type_2","map")
  //    	val dat = gg.join(nn, 'type === 'type_2).drop('type_2)
     	
     	// Formating
     	// val dd = counting_pres.map(x=> (x._1,x._2.view map{
     	// 	case(key,value) => "\n\t" + key + ":" + value
     	// } mkString ("", "", "\n")))

     	// val joined_data = counts.join(counting_pres)



     	// val sorted_counts = counts.sortBy(_._2) 
     	
		// val bgcount = words.map(bg => (bg.firstWord, bg.secondWord))
     	// val tes = bgcount.reduceByKey()
     												
     	// val filtered_ = words.filter(!_.isEmpty)
		 // val wd = words.map(_.sliding(2).map{case Array(e1,e2) => (e1,e2)})	
		  // println(wd mkString "\n")										
      	// Transform into word and count.
      	// val wordy = words.map{ x => x.tail.head -> x.head}
      	// val counts = words.map(word => (word, 1))//.reduceByKey{case (x, y) => x + y}
      	// Save the word count back out to a text file, causing evaluation.


      	sorted_list.saveAsTextFile(outputFile)
		// converted_toString.saveAsTextFile(output_File)

		val et = (System.currentTimeMillis - t0) / 1000
		System.err.println("Done!\nTime taken = %d mins %d secs".format(et / 60, et % 60))
	}
}
